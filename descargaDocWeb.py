import urllib.request


class Robot:
    def _init_(self, url):
        self.url = url
        self.downloaded = False
        self.document = ""

    def retrieve(self):
        if not self.downloaded:
            print("Descargando", self.url)
            response = urllib.request.urlopen(self.url)
            self.document = response.read().decode()
            self.downloaded = True

    def show(self):
        self.retrieve()
        print(self.document)

    def content(self):
        self.retrieve()
        return self.document


class Cache:
    def _init_(self):
        self.documents = {}

    def retrieve(self, url):
        if url not in self.documents:
            robot = Robot(url)
            robot.retrieve()
            self.documents[url] = robot.content()

    def show(self, url):
        self.retrieve(url)
        print(self.documents[url])

    def show_all(self):
        for url in self.documents:
            print(url)

    def content(self, url):
        self.retrieve(url)
        return self.documents[url]


# Programa principal
if __name__ == "_main_":
    # Crear objetos Robot
    robot1 = Robot("https://cursosweb.github.io/programas/IT-ST.pdf")
    robot2 = Robot("https://gsyc.urjc.es//")

    # Llamar a métodos de Robot
    robot1.retrieve()
    robot1.show()
    content = robot1.content()
    print(content)

    # Crear objetos Cache
    cache1 = Cache()
    cache2 = Cache()

    # Llamar a métodos de Cache
    cache1.retrieve("https://cursosweb.github.io/programas/IT-ST.pdf")
    cache1.show("https://cursosweb.github.io/programas/IT-ST.pdf")
    cache2.retrieve("https://gsyc.urjc.es//")
    cache2.show("https://gsyc.urjc.es//")
    cache2.show_all()
    content = cache2.content("https://cursosweb.github.io/programas/IT-ST.pdf")
    print(content)
